//
// Created by Egor Safronov on 30.12.2021.
//

#include "test.h"
#include "mem.h"
#include "mem_internals.h"


void test1() {
    printf("%s\n", "[malloc]");
    void *heap = heap_init(5000);
    debug_heap(stdout, heap);
    printf("%s\n", "allocated (500): ");
    _malloc(500);
    debug_heap(stdout, heap);
    _free(heap);
    printf("\n\n\n");
}

void test2() {
    printf("%s\n", "[free one block]");

    void *heap = heap_init(5000);
    void *block_to_free = _malloc(1000);

    debug_heap(stdout, heap);
    printf("%s\n", "freed (1000): ");
    _free(block_to_free);
    debug_heap(stdout, heap);
    _free(heap);
    printf("\n\n\n");
}

void test3() {
    printf("%s\n", "[free few blocks case]");

    void *heap = heap_init(5000);
    void *block_to_free_1 = _malloc(1000);
    void *block_to_free_2 = _malloc(500);
    void *block_to_free_3 = _malloc(300);

    debug_heap(stdout, heap);
    printf("%s\n", "block2 (500) freed: ");
    _free(block_to_free_2);
    debug_heap(stdout, heap);
    printf("%s\n", "block1 (1000) freed: ");
    _free(block_to_free_1);
    debug_heap(stdout, heap);
    printf("%s\n", "block3 (300) freed: ");
    _free(block_to_free_3);
    debug_heap(stdout, heap);
    _free(heap);
    printf("\n\n\n");
}

void test4() {
    printf("%s\n", "[extend memory]");

    void *heap = heap_init(10000);
    _malloc(7000);

    debug_heap(stdout, heap);
    printf("%s\n", "malloc (+3000): ");
    _malloc(3000);
    debug_heap(stdout, heap);
    printf("\n\n\n");
}

void test5() {
    printf("%s\n", "[allocate new memory and move]");

    void *heap1 = heap_init(5000);
    debug_heap(stdout, heap1);

    struct block_header* tmp = (struct block_header*) heap1 + 8000;
    void *mmap_result = mmap((void*) tmp, 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED_NOREPLACE, 0, 0);

    printf("%s\n", "result: ");
    _malloc(9000);
    debug_heap(stdout, heap1);
    _free(heap1);
    _free(mmap_result);
    printf("\n\n\n");
}
